package com.company.logic5;

import java.util.Scanner;

public class Main {
    public Main() {
    }
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String answer = "Y";

        while(answer.toUpperCase().equals("Y"))
        {
            System.out.println("Please enter the question number: ");
            int number = input.nextInt();

            switch (number)
            {
                case 1:
                    logic05.Soal01.Resolve();
                    break;
                case 2:
                    logic05.Soal02.Resolve();
                    break;
                case 3:
                    logic05.Soal03.Resolve();
                    break;
                case 4:
                    logic05.Soal04.Resolve();
                    break;
                case 5:
                    logic05.Soal05.Resolve();
                    break;
                case 6:
                    logic05.Soal06.Resolve();
                    break;
                case 7:
                    logic05.Soal07.Resolve();
                    break;
                case 8:
                    logic05.Soal08.Resolve();
                    break;
                case 9:
                    logic05.Soal09.Resolve();
                    break;
                case 10:
                    logic05.Soal10.Resolve();
                    break;



                default:
                    System.out.println("No question available");
                    break;
            }

            System.out.println("Again? (Y/N)");
            input.nextLine();
            answer = input.nextLine();
        }

        System.out.println("Thank you. See you soon");

    }
}
