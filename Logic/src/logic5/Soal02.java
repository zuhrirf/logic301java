//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package logic05;

import java.util.Arrays;
import java.util.Scanner;

public class Soal02 {
    public Soal02() {
    }

    public static void Resolve() {
        char[] vokal = new char[]{'a', 'i', 'u', 'e', 'o'};
        char[] konsonan = new char[]{'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'};
        Scanner input = new Scanner(System.in);
        System.out.println("Soal 02: Vokal dan Konsonan");
        System.out.print("Masukan kalimat : ");
        String stringInput = input.nextLine();
        String stringConvert = stringInput.toLowerCase();
        char[] charArray = stringConvert.toCharArray();
        Arrays.sort(charArray);
        String vokalString = "";
        String konsonanString = "";

        for(int i = 0; i < charArray.length; i++) {
            int j;
            for(j = 0; j < vokal.length; j++) {
                if (vokal[j] == charArray[i]) {
                    vokalString = vokalString + charArray[i];
                }
            }

            for(j = 0; j < konsonan.length; j++) {
                if (konsonan[j] == charArray[i]) {
                    konsonanString = konsonanString + charArray[i];
                }
            }
        }

        System.out.print("Huruf Vokal: " + vokalString);
        System.out.println();
        System.out.print("\nHuruf Konsonan: " + konsonanString);
    }
}
