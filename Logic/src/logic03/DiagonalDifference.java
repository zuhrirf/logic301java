package com.company.logic03;
import java.util.Scanner;

public class DiagonalDifference {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukan nilai n = ");
        int n = Integer.parseInt(input.nextLine());
        int diagonal1 = 0;
        int diagonal2 = 0;
        for (int i=0; i<n; i++){
            String[] matriks = input.nextLine().split(" ");
            diagonal1 = diagonal1 + Integer.parseInt(matriks[i]);
            diagonal2 = diagonal2 + Integer.parseInt(matriks[n-1-i]);
        }
        int hasil;
                hasil = Math.abs(diagonal1 - diagonal2);
        System.out.print(hasil);
        System.out.println();

    }

}
