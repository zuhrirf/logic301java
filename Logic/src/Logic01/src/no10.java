package com.company.Logic01.src;

import java.util.Scanner;
public class no10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Nomor 06
        // Prompt input
        System.out.println("Please enter the value of n: ");
        int n = input.nextInt();

        // Prerequisite
        int[] arrayInteger = new int[n];
        int helper = 3;

        // Enter the value to array variable
        for (int i = 0; i < n; i++) {
            arrayInteger[i] = helper;
            helper *= 3;
        }

        // Print array
        for (int i = 0; i < n; i++) {
            if(i==3) {
                System.out.print("XXX ");
            }
            else {
                System.out.print(arrayInteger[i] + " ");
            }
        }

        System.out.println();

    }
}