package DimPractice1;

import java.util.Scanner;

public class ForNum6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		// Nomor 06
		System.out.println("Nomor 06");
		// Prompt input
		System.out.print("Input value of n:");
		int n = input.nextInt();

        // Prerequisite
        int[] arrayInteger = new int[n];
        int helper = 1;

        // Enter the value to array variable
        for (int i = 0; i < n; i++) {
            arrayInteger[i] = helper;
            helper += 4;
        }

        // Print array
        for (int i = 0; i < n; i++) {
        	if(i==2 || i==5) {
        		System.out.print(" * ");
        	}
        	else {
        		System.out.print(arrayInteger[i] + " ");
        	}
        }

        System.out.println();
        input.close();
	}

}
