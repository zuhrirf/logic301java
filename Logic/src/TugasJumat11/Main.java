package com.company.TugasJumat11;
import java.util.Scanner;
public class Main {
    public Main() {
    }
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String answer = "Y";

        while(answer.toUpperCase().equals("Y"))
        {
            System.out.println("Please enter the question number: ");
            int number = input.nextInt();

            switch (number)
            {
                case 1:
                    TugasJumat11.Soalke1.Resolve();
                    break;
                case 2:
                    TugasJumat11.Soalke2.Resolve();
                    break;
                case 3:
                    TugasJumat11.Soalke3.Resolve();
                    break;
                case 4:
                    TugasJumat11.Soalke4.Resolve();
                    break;
                case 5:
                    TugasJumat11.Soalke5.Resolve();
                    break;
                case 6:
                    TugasJumat11.Soalke6.Resolve();
                    break;
                case 7:
                    TugasJumat11.Soalke7.Resolve();
                    break;
                case 8:
                    TugasJumat11.Soalke8.Resolve();
                    break;
                case 9:
                    TugasJumat11.Soalke9.Resolve();
                    break;
                case 10:
                    TugasJumat11.Soalke10.Resolve();
                    break;
                case 11:
                    TugasJumat11.Soalke11.Resolve();
                    break;
                case 12:
                    TugasJumat11.Soalke12.Resolve();
                    break;
                case 13:
                    TugasJumat11.Soalke13.Resolve();
                    break;
                default:
                    System.out.println("No question available");
                    break;
            }

            System.out.println("Again? (Y/N)");
            input.nextLine();
            answer = input.nextLine();
        }

        System.out.println("Thank you. See you soon");

    }
}
