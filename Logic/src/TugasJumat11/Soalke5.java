package TugasJumat11;
import java.util.Scanner;
public class Soalke5 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.print("Konversi Waktu");
        System.out.print("Masukan Jam (Ex: 13:30 ) : ");
        String StringJam = input.nextLine(); //Input waktu
        String[] ArrayStringJam = StringJam.split(" "); //Split stringjam antara AM/PM dan Jam

        if(ArrayStringJam.length==1){ //Kondisi ketika inputan panjangnya 1 maka untuk 12H ke 24H
            String StringJam24 = ArrayStringJam[0];
            String[] ArrayStringJam24 = StringJam24.split(":"); //Split Waktu berdasarkan ":"

            int jam24H = Integer.parseInt(ArrayStringJam24[0]); // Mengembalikan nilai jam string ke int
            int menit24H = Integer.parseInt(ArrayStringJam24[1]); // Mengembalikan nilai menit string ke int

            if (jam24H < 12 && menit24H <= 59) {
                System.out.print("Output : " + jam24H + ":" + menit24H + " AM");
            } else if (jam24H > 12 && jam24H <= 23 && menit24H <= 59) {
                System.out.print("Output : " + (jam24H - 12) + ":" + menit24H + " PM");
            } else if ((jam24H > 24) || (menit24H > 59)) {
                System.out.print("Format tidak valid");
            }
        } else{ //Kondisi ketika inputan panjangnya 1 maka untuk 24H ke 12H
            String StringJam12 = ArrayStringJam[0];
            String[] ArrayStringJam12 = StringJam12.split(":"); //Split Waktu berdasarkan ":"

            int jam12H = Integer.parseInt(ArrayStringJam12[0]); // Mengembalikan nilai jam string ke int
            int menit12H = Integer.parseInt(ArrayStringJam12[1]); // Mengembalikan nilai menit string ke int

            String StringAM = "AM";
            String StringPM = "PM";

            String AMPM = ArrayStringJam[1]; // Menyimpan string AM/PM

            if (jam12H < 12 && menit12H <= 59 && AMPM.toLowerCase().equals(StringAM.toLowerCase())) {
                System.out.print("Output : " + jam12H + ":" + menit12H);
            } else if (jam12H == 12 && menit12H <= 59 && AMPM.toLowerCase().equals(StringAM.toLowerCase())) {
                System.out.print("Output : " + "00" + ":" + menit12H);
            } else if (jam12H < 12 && menit12H <= 59 && AMPM.toLowerCase().equals(StringPM.toLowerCase())) {
                System.out.print("Output : " + (jam12H + 12) + ":" + menit12H);
            } else if ((jam12H > 12) || (menit12H > 59)) {
                System.out.print("Format tidak valid");
            }
        }
    }
}