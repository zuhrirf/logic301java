package TugasJumat11;
import java.util.Scanner;

public class Soalke1 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.print("Input panjang deret = ");
        int batasan = input.nextInt();

        //Definisikan variabel
        int bilKel3 = 3; //Kelipatan 3
        int bilKel2 = -2; //Kelipatan (-2)*1
        int helper = 0;

        //Definisikan array
        int[] arrayKel3 = new int[batasan];
        int[] arrayKel3Minus1 = new int[batasan];
        int[] arrayKel2 = new int[batasan];
        int[] arraySum = new int[batasan];

        //Deret Kelipatan 3
        int i;
        for(i = 0; i < arrayKel3.length; ++i) {
            arrayKel3[i]=bilKel3;
            bilKel3+=3;
        }
        //Deret Kelipatan 3-1
        for(i = 0; i < arrayKel3Minus1.length; ++i) {
            arrayKel3Minus1[i]=arrayKel3[i]-1;
            System.out.print(arrayKel3Minus1[i]+" ");
        }
        System.out.println();
        //Deret Kelipatan (-2)*1
        for(i = 0; i < arrayKel2.length; ++i) {
            System.out.print(bilKel2+" ");
            arrayKel2[i]=bilKel2;
            bilKel2+=(-2)*1;
        }
        System.out.println("\n");
        System.out.println("Output : ");
        for(i = 0; i < arrayKel3Minus1.length; ++i) {
            arraySum[i] = arrayKel3Minus1[i] + arrayKel2[i]; //Menghitung penjumlahan indeks
            System.out.print(arraySum[i] + " ");
        }
    }
}
