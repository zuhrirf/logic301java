package test;

import java.util.Scanner;

public class soal4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("masukan jam :");
		String input = sc.nextLine();

		int jam = Integer.parseInt(input.substring(0, 2));
		String ap = input.substring(input.length() - 2);
		String cetak = "";

		if (ap.equalsIgnoreCase("am")) {
			if (jam == 12) {
				cetak = "00" + ":" + input.substring(3, 5);
			} else {
				cetak = "0" + jam + ":" + input.substring(3, 5);
			}
		} else if (ap.equalsIgnoreCase("pm")) {

			if (jam == 12) {
				cetak = "00" + ":" + input.substring(3, 5);
			} else {
				cetak = (jam + 12) + ":" + input.substring(3, 5);
			}
		} else if (jam == 0 || jam == 24) {
			cetak = "12" + ":" + input.substring(3, 5) + " AM";
		} else if (jam >= 12) {
			if (jam >= 22) {
				cetak = (jam - 12) + ":" + input.substring(3, 5) + "PM";
			} else {
				cetak = "0" + (jam - 12) + ":" + input.substring(3, 5) + "PM";
			}
		} else if (jam == 12) {
			cetak = jam + ":" + input.substring(3, 5) + "PM";
		} else if (jam >= 10) {
			cetak = jam + ":" + input.substring(3, 5) + "AM";
		} else {
			cetak = "0" + jam + ":" + input.substring(3, 5) + "AM";
		}
		System.out.println("output : " + cetak);
	}

}
